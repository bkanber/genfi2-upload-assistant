This is the download site for the GENFI2 Upload Assistant. 

*If you are upgrading your GENFI2 Upload Assistant, please uninstall the previous version first for a proper upgrade.*

**Version History**

v1.1.2 Made the upload server url selectable between genfi2 and genfi2test